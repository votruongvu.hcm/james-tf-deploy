resource "google_container_cluster" "primary" {
  name               = var.cluster_name
  location           = var.environment.zone
  remove_default_node_pool = true
  initial_node_count = var.initial_node_count 
  network = var.network

  master_auth {
    username = var.auth.username
    password = var.auth.password

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  timeouts {
    create = "30m"
    update = "40m"
  }

  resource_labels = {
    environment = var.environment.name
  }
}
