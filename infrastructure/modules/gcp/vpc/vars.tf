variable "name" {
  description = "Network name"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}
