resource "google_compute_network" "ntw" {
  name                    = var.name
  auto_create_subnetworks = "true"
}