resource "google_compute_global_address" "address" {
  name = var.name
  ip_version = "IPV4"
  project = var.environment.project
}
