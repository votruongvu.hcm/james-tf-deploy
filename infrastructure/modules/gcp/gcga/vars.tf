variable "name" {}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}