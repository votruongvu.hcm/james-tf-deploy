resource "google_container_node_pool" "np" {
  name       = var.node_name
  location   = var.environment.zone
  cluster    = var.cluster_name
  node_count = var.node_count

  node_config {
    preemptible = false
    metadata = {
      disable-legacy-endpoints = "true"
    }
    image_type   = var.image_type
    machine_type = var.machine_type
    disk_size_gb = var.disk_size_gb

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_write",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      environment = var.environment.name
    }
  }
}
