variable "name" { 
  default     = "leo-web-stag"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}
