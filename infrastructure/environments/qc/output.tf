output "static_ip" {
  value = module.base.static_ip
}

output "cluster_info" {
  value = module.base.cluster_info
}

output "node_name" {
  value = module.base.node_name
}

output "kubconfig" {
  value = module.base.kubconfig
}