variable "name" { 
  default     = "leo-web-qc"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}
