variable "name" { 
  default     = "leo-web-dev"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}
