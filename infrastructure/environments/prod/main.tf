terraform {
  backend "gcs" {
    #create a bucket as this name before run terraform commands
    bucket = "leo-web-infra-prod"
    prefix  = "terraform/state"    
  }
}

provider "google" {
  version = "3.5.0"

  project = var.environment.project
  region  = var.environment.region
  zone    = var.environment.zone
}

module "base" {
  source = "../base"
  name = var.name
  environment = var.environment
}
