variable "name" { 
  default     = "leo-web-prod"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}
