output "static_ip" {
  value = module.static_ip.instance
}

output "cluster_info" {
  value = module.cluster.cluster_info
}

output "node_name" {
  value = module.node_pool.name
}

output "kubconfig" {
  value = format("%s%s%s%s%s%s", " ... \nRun command to configure access via kubectl:\n$ gcloud container clusters get-credentials", module.cluster.cluster_info.name, "--zone ", var.environment.zone, "--project ", var.environment.project)
}