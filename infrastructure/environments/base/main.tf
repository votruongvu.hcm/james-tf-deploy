module "vpc" {
  source = "../../modules/gcp/vpc"
  name = format("%s%s", var.name, "-vpc") 
  environment  = var.environment
}

resource "random_uuid" "username" { }
resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_%@"
}

module "cluster" {
  source = "../../modules/gcp/cluster"
  cluster_name = format("%s%s", var.name, "-cluster")
  network = module.vpc.name
  environment  = var.environment
  auth = {
    username = random_uuid.username.result
    password = random_password.password.result
  }
}

module "node_pool" {
  source = "../../modules/gcp/node-pool"
  node_name = format("%s%s", var.name, "-node-pool")
  cluster_name = module.cluster.cluster_info.name
  environment  = var.environment
}

module "fw_ssh" {
  source      = "../../modules/gcp/firewall/ingress-allow"
  name        = format("%s%s", var.name, "-allow-ssh")
  network     = module.vpc.name
  protocol    = "tcp"
  ports       = ["22"]
  environment  = var.environment
}

module "static_ip" {
  source = "../../modules/gcp/gcga"
  name = format("%s%s", var.name, "-static-ip")
  environment  = var.environment
}