resource "kubernetes_storage_class" "sc" {
  metadata {
    name = "${var.name}-sc"
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  reclaim_policy = "Retain"
  parameters = {
    type = var.pd.type
    zone = var.environment.zone
  }
}

resource "kubernetes_persistent_volume_claim" "pvc" {
  metadata {
    name = "${var.name}-pvc"
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  spec {
    access_modes = var.access_mode
    resources {
      requests = {
        storage = var.pd.size
      }
    }
    storage_class_name = kubernetes_storage_class.sc.metadata[0].name
  }
}
