variable "name" {}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "namespace" {
  type = object({
    metadata = list(object({
      name = string
      labels = object({
        environment = string
      })
    }))
  })
}

variable "pd" {
  type = object({
    size = string
    type = string
  })
  default = {
    size = "5Gi"
    type = "pd-standard"
  }
}

variable "access_mode" {
  default = [
    "ReadWriteOnce"
  ]
}
