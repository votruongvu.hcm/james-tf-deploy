output "sc" {
  value = kubernetes_storage_class.sc
}

output "pvc" {
  value = kubernetes_persistent_volume_claim.pvc
}
