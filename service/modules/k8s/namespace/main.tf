resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.name
    annotations = {
      name = var.name
    }
    labels = {
      environment = var.environment.name
    }
  }
}
