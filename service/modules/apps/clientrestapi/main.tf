resource "kubernetes_config_map" "config" {
  metadata {
    name = format("%s%s", var.name, "-config-map")
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  data = {
    GCP_PROJECT_ID = var.environment.project
  }
}

resource "kubernetes_secret" "secret" {
  metadata {
    name = format("%s%s", var.name, "-secret")
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  data = {
    DATABASE_NAME = var.cloudsql.databasename
    DATABASE_USER = var.cloudsql.databaseusername
    DATABASE_PASSWORD = var.cloudsql.databasepassword
    DJANGO_SECRET_KEY = var.cloudsql.djangosecretkey
  }
}

resource "kubernetes_deployment" "deployment" {
  metadata {
    name = var.name
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.name
      }
    }
    template {
      metadata {
        labels = {
          app = var.name
          environment = var.namespace.metadata[0].labels.environment
        }
      }
      spec {
        security_context {
          run_as_non_root = true
          run_as_user = 65532
          run_as_group = 65532
        }
        container {
          name = var.name
          image = "${var.image.registry}/${var.image.name}:${var.image_tag}"
          env_from {
            config_map_ref {
              name = kubernetes_config_map.config.metadata[0].name
            }
          }
          env_from {
            secret_ref {
              name = kubernetes_secret.secret.metadata[0].name
            }
          }
          port {
            container_port = var.hosting.port
          }
        }
        container {
          name = "cloudsql-proxy"
          image = "${var.gce_proxy_image.registry}/${var.gce_proxy_image.name}:${var.gce_proxy_image.image_tag}"
          volume_mount {
            name = "cloudsql-oauth-credentials"
            mount_path = "/secrets/cloudsql"
            read_only = true
          }
          volume_mount {
            name = "cloudsql"
            mount_path = "/cloudsql"
          }
          command =  ["/cloud_sql_proxy", "--dir=/cloudsql",
                  format("%s%s", "-instances=", var.cloudsql_proxy_command.instances),
                  format("%s%s", "-credential_file=/secrets/cloudsql/", var.cloudsql_proxy_command.credential_file)]
          port {
            container_port = var.hosting.port
          }
        }
        volume {
          name = "cloudsql-oauth-credentials"
          secret {
              secret_name = var.cloudsql_oauth_credentials_secret_name
          }
        }
        volume {
          name = "cloudsql"
          persistent_volume_claim {
              claim_name = var.cloudsql_proxy_pvc.cloudsql
          }
        }
      }     
    }
  }
}

resource "kubernetes_service" "service" {
  metadata {
    name = var.name
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  spec {
    selector = {
      app = var.name
    }
    port {
      port = var.service.port
      target_port = var.hosting.port
      protocol = "TCP"
    }
    type = "NodePort"
  }
}
