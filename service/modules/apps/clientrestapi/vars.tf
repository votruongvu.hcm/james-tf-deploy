variable "name" {
  default="leonardoapi"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "namespace" {
  type = object({
    metadata = list(object({
      name = string
      labels = object({
        environment = string
      })
    }))
  })
}

variable "cloudsql" {
  type = object({
      databasename = string
      databaseusername = string
      databasepassword = string
      djangosecretkey = string
  })
}

variable "cloudsql_proxy_pvc" {
  type = object({
      cloudsql = string
  })
}

variable "image" {
  type = object({
    registry = string
    name = string
  })
}
variable "image_tag" {  
}

variable "gce_proxy_image" {
  type = object({
    registry = string
    name = string
    image_tag = string
  })
}

variable "cloudsql_proxy_command" {
  type = object({
      instances = string
      credential_file = string
  })
}

variable "hosting" {
  type = object({
    port = number
  })
  default = {
    port = 8000
  }
}


variable "service" {
  type = object({
    port = number
  })
  default = {
    port = 80
  }
}

variable "cloudsql_oauth_credentials_secret_name" {
  
}
