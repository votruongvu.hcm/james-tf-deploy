resource "kubernetes_deployment" "deployment" {
  metadata {
    name = var.name
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = var.name
      }
    }
    template {
      metadata {
        labels = {
          app = var.name
          environment = var.namespace.metadata[0].labels.environment
        }
      }
      spec {
        container {
          name = var.name
          image = "${var.image.registry}/${var.image.name}:${var.image_tag}"
          image_pull_policy = "Always"          
          port {
            container_port = var.hosting.port
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "service" {
  metadata {
    name = var.name
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  spec {
    selector = {
      app = var.name
    }
    port {
      name = var.name
      port = var.hosting.port
      protocol = "TCP"
    }
    type = "NodePort"
  }
}
