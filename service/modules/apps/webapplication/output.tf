output "instance" {
  value = {
    host = kubernetes_service.service.metadata[0].name
    port = kubernetes_service.service.spec[0].port[0].port
    url = format("%s%s%s%s%s", "http://", kubernetes_service.service.metadata[0].name, kubernetes_service.service.metadata[0].namespace, ":", kubernetes_service.service.spec[0].port[0].port)
  }
}
