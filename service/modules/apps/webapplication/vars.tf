variable "name" {
  default = "leonardoweb"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "namespace" {
  type = object({
    metadata = list(object({
      name = string
      labels = object({
        environment = string
      })
    }))
  })
}

variable "image" {
  type = object({
    registry = string
    name = string
  })  
}

variable "image_tag" {
}

variable "hosting" {
  type = object({
    port = number
  })
  default = {
    port = 80
  }
}