module "namespace" {
  source = "../../modules/k8s/namespace"
  name = format("%s%s", var.name, "-ns") 
  environment  = var.environment
}



# module "ssl_certs_pvc_pd" {
#   source = "../../modules/k8s/pvc/pd"
#   name = format("%s%s", var.name, "-ssl-certs-pvc-pd") 
#   environment = var.environment
#   namespace = module.namespace.instance
#   pd = var.pd
# }

module "cloud_sql_pvc_pd" {
  source = "../../modules/k8s/pvc/pd"
  name = format("%s%s", var.name, "-cloud-sql-pvc-pd") 
  environment = var.environment
  namespace = module.namespace.instance
  pd = var.pd
}

module "client_rest_api" {
  source = "../../modules/apps/clientrestapi"
  name = format("%s%s", var.name, "-client-rest-api") 
  environment  = var.environment
  namespace = module.namespace.instance
  cloudsql = var.cloudsql
  image = var.client_rest_api_image
  image_tag = var.client_rest_api_image_tag
  gce_proxy_image = var.gce_proxy_image
  cloudsql_proxy_command = var.cloudsql_proxy_command
  cloudsql_proxy_pvc = {
    cloudsql = module.cloud_sql_pvc_pd.pvc.metadata[0].name
  }
  cloudsql_oauth_credentials_secret_name = var.cloudsql_oauth_credentials_secret_name
} 

module "web_application" {
  source = "../../modules/apps/webapplication"
  name = format("%s%s", var.name, "-web-application") 
  environment  = var.environment
  namespace = module.namespace.instance
  image = var.web_application_image
  image_tag = var.web_application_image_tag
}