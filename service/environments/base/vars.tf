variable "name" {
  default = "leo"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "pd" {
  type = object({
    size = string
    type = string
  })
}

variable "cloudsql" {
  type = object({
      databasename = string
      databaseusername = string
      databasepassword = string
      djangosecretkey = string
  })
}

variable "client_rest_api_image" {
  type = object({
    registry = string
    name = string
  })
}

variable "client_rest_api_image_tag" {
  
}


variable "web_application_image" {
  type = object({
    registry = string
    name = string
  })
}

variable "web_application_image_tag" {
}

variable "gce_proxy_image" {
  type = object({
    registry = string
    name = string
    image_tag = string
  })
  default = {
    registry = "gcr.io/cloudsql-docker"
    name = "gce-proxy"
    image_tag = "1.19.1"
  }
}

variable "cloudsql_proxy_command" {
  type = object({
      instances = string
      credential_file = string
  })
}


variable "cloudsql_oauth_credentials_secret_name" {

}

