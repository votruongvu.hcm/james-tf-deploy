output "client_rest_api" {
    value = module.client_rest_api.instance
}

output "web_application" {
    value = module.web_application.instance
}

output "namespace" {
    value = module.namespace.instance
}