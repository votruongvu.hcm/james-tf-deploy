 resource "kubernetes_secret" "cloudsql_oauth_credentials" {
  metadata {
    name =  format("%s%s", var.name, "-cloud-sql-oauth-credentials-secret") 
    namespace =  var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }

  data = {
    "credentials.json" =  file(var.cloud_sql_oauth_credentials_file_path)
  }
}

 resource "kubernetes_secret" "tls_secret" {
  metadata {
    name = format("%s%s", var.name, "-tls-secret")
    namespace = var.namespace.metadata[0].name
    labels = {
      app = var.name
      environment = var.namespace.metadata[0].labels.environment
    }
  }
  data = {
    "tls.crt" = file(var.crt_file_path)
    "tls.key" = file(var.key_file_path)
  }
}