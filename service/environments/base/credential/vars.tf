variable "name" {
  default = "app"
}

variable "namespace" {
  type = object({
    metadata = list(object({
      name = string
      labels = object({
        environment = string
      })
    }))
  })
}

variable "crt_file_path" {
  
}

variable "key_file_path" {

}

variable "cloud_sql_oauth_credentials_file_path" {
  
}