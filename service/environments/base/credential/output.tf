output "cloudsql_oauth_credentials_secret_name" {
  value = kubernetes_secret.cloudsql_oauth_credentials.metadata[0].name
}

output "tls_secret_name" {
  value = kubernetes_secret.tls_secret.metadata[0].name
}