variable "name" {
  default = "app"
}

variable "environment" {
  type = object({
    name = string
  })
}

variable "namespace" {
  type = object({
    metadata = list(object({
      name = string
      labels = object({
        environment = string
      })
    }))
  })
}

variable "address_static_ip" {
}

variable "secret_name" {
}

variable "leonardoapi" {
   type = object({
    service = object({
      name = string
      port = string
    })
    hosting  = object({
      url = string
    })
  })
}

variable "leonardoweb" {
   type = object({
    service = object({
      name = string
      port = string
    })
    hosting  = object({
      url = string
    })
  })
}