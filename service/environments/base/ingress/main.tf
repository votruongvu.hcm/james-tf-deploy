resource "kubernetes_ingress" "ingress" {
  metadata {
    name = var.name
    namespace = var.namespace.metadata[0].name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = var.address_static_ip.name
    }
  }

  spec {
    tls {
      hosts = [ var.leonardoapi.hosting.url, var.leonardoweb.hosting.url]
      secret_name = var.secret_name
    }
    rule {
      host =  var.leonardoapi.hosting.url
      http {
        path {
          backend {
            service_name = var.leonardoapi.service.name
            service_port = var.leonardoapi.service.port
          }
        }
      }
    }

    rule {
      host =  var.leonardoweb.hosting.url
      http {
        path {
          backend {
            service_name = var.leonardoweb.service.name
            service_port = var.leonardoweb.service.port
          }
        }
      }
    }
  }
}