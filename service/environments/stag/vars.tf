variable "name" {
  default = "leo-web-stag"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "pd" {
  type = object({
    size = string
    type = string
  })
  default = {
    size = "10Gi"
    type = "pd-standard"
  }
}

variable "cloudsql" {
  type = object({
      databasename = string
      databaseusername = string
      databasepassword = string
      djangosecretkey = string
  })
}

variable "client_rest_api_image" {
  type = object({
    registry = string
    name = string
  })
  default = {
    registry = "us.gcr.io/leo-container-registry"
    name = "leo-client"
  }
}

variable "client_rest_api_image_tag" {
  default = "latest"
}


variable "web_application_image" {
  type = object({
    registry = string
    name = string
  })
  default = {
    registry = "us.gcr.io/leo-container-registry"
    name = "leo-angular"
  }
}

variable "web_application_image_tag" {
  default = "latest"
}

variable "gce_proxy_image" {
  type = object({
    registry = string
    name = string
    image_tag = string
  })
  default = {
    registry = "gcr.io/cloudsql-docker"
    name = "gce-proxy"
    image_tag = "1.19.1"
  }
}

variable "cloudsql_proxy_command" {
  type = object({
      instances = string
      credential_file = string
  })
}

variable client_rest_api_url {
    default = "api-stag-leo.tpptechnology.com"
}

variable "web_application_url" {
    default = "web-stag-leo.tpptechnology.com"
}

variable "crt_file_path" {
    default = "Server.crt"
}

variable "key_file_path" {
    default = "Server.key"
}

variable "cloud_sql_oauth_credentials_file_path" {
  default = "cloud_sql_oauth.json"
}