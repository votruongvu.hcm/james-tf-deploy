provider "google" {
  version = "3.5.0"

  project = var.environment.project
  region  = var.region
}

data "terraform_remote_state" "infrastructure" {
  backend = "gcs"
  config = {
    bucket = "leo-web-infra-qc"
    prefix  = "terraform/state"   
  }
}

provider "kubernetes" {
  host = data.terraform_remote_state.infrastructure.outputs.cluster_info.endpoint
  username = data.terraform_remote_state.infrastructure.outputs.cluster_info.master_auth[0].username
  password = data.terraform_remote_state.infrastructure.outputs.cluster_info.master_auth[0].password
  insecure = true
  load_config_file = false
}

terraform {
  backend "gcs" {
    bucket = "leo-web-app-qc"
    prefix  = "terraform/state"    
  }
}

module "credential" {
  source = "../base/credential"
  name = var.name
  namespace = module.base.namespace
  crt_file_path = var.crt_file_path
  key_file_path = var.key_file_path
  cloud_sql_oauth_credentials_file_path = var.cloud_sql_oauth_credentials_file_path
}

module "base" {
  source = "../base"
  name = var.name
  environment = var.environment
  pd = var.pd
  cloudsql = var.cloudsql
  cloudsql_proxy_command = var.cloudsql_proxy_command
  web_application_image = var.web_application_image
  client_rest_api_image = var.client_rest_api_image
  client_rest_api_image_tag = var.client_rest_api_image_tag
  web_application_image_tag = var.web_application_image_tag
  cloudsql_oauth_credentials_secret_name = module.credential.cloudsql_oauth_credentials_secret_name
}




module "ingress" {
  source = "../base/ingress"
  name = var.name
  namespace = module.base.namespace
  environment = var.environment
  address_static_ip = data.terraform_remote_state.infrastructure.outputs.static_ip
  secret_name = module.credential.tls_secret_name
  leonardoapi = {
    service = {
      name = module.base.client_rest_api.host
      port = module.base.client_rest_api.port
    }
    hosting = {
      url = var.client_rest_api_url
    }
  }
  leonardoweb =  {
    service = {
      name = module.base.web_application.host
      port = module.base.web_application.port
    }
    hosting = {
      url = var.web_application_url
    }
  }
  depends_on = [ module.base , module.credential ]
}




