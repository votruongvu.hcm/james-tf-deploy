variable "name" {
  default = "leo-web-qc"
}

variable "environment" {
  type = object({
    name = string
    project = string
    region = string
    zone = string
  })
}

variable "pd" {
  type = object({
    size = string
    type = string
  })
  default = {
    size = "10Gi"
    type = "pd-standard"
  }
}

variable "cloudsql" {
  type = object({
      databasename = string
      databaseusername = string
      databasepassword = string
      djangosecretkey = string
  })
}

variable "client_rest_api_image" {
  type = object({
    registry = string
    name = string
  })
}

variable "client_rest_api_image_tag" {
}


variable "web_application_image" {
  type = object({
    registry = string
    name = string
  })
}

variable "web_application_image_tag" {
}

variable "gce_proxy_image" {
  type = object({
    registry = string
    name = string
    image_tag = string
  })
}

variable "cloudsql_proxy_command" {
  type = object({
      instances = string
      credential_file = string
  })
}

variable client_rest_api_url {
}

variable "web_application_url" {
}

variable "crt_file_path" {
}

variable "key_file_path" {
}

variable "cloud_sql_oauth_credentials_file_path" {
}